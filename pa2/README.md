The project has two dependencies that need to be installed: BeautifulSoup and pylcs, both from pip.
The program is then run with python run-extraction.py \<MODE\>

Modes are: A - regex extraction, B- Xpath extraction, E- Wrapper generation

Everything gets printed to the standard output.