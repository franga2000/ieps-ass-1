import re

def extract_data_from_rtvslo(html):
    title = re.findall(r'<h1>(.*?)</h1>', html)
    subtitle = re.findall(r'<div class="subtitle">(.*?)</div>', html)
    lead = re.findall(r'<p class="lead">(.*?)</p>', html)
    author = re.findall(r'<div class="author-name">(.*?)</div>', html)
    publish_date = re.findall(r'<div class="publish-meta">(.*?)<br>', html, re.DOTALL)
    content = re.findall(r'<p class="Body">(.*?)</p>', html, re.DOTALL)
    highlights = re.findall(r'<li>(.*?)</li>', html)
    
    key_data = re.findall(r'<p><strong>Ključni tehnični podatki:</strong></p>(.*?)<div class="gallery">', html, re.DOTALL)
    
    print( 
        {
            'title': title[0] if title else None,
            'subtitle': subtitle[0] if subtitle else None,
            'lead': lead[0] if lead else None,
            'author': author[0] if author else None,
            'publish_date': publish_date[0].strip() if publish_date else None,
            'highlights': highlights if highlights else None,
            'key_data': key_data[0] if key_data else None,
            'content': content if content else None
        })
    
def extract_data_from_overstock(html):
    list_prices = re.findall(r'<s>(.*?)</s>', html)
    actual_prices = re.findall(r'<span class="bigred"><b>(.*?)</b></span>', html)
    item_descriptions = re.findall(r'<span class="normal">(.*?)<br>', html, re.DOTALL)
    item_titles = re.findall(r'<a href=".*?PROD_ID.*"><b>(.*?)</b></a>', html)
    for i in range(len(item_descriptions)):
        print(
            {
                'item_title': item_titles[i],
                'list_price': list_prices[i],
                'actual_price': actual_prices[i],
                'item_description': item_descriptions[i]
            }
        )

def extract_data_from_arstechnica(html):
    title = re.findall(r'<h1 itemprop="headline">(.*?)</h1>', html)
    subtitle = re.findall(r'<h2 itemprop="description">(.*?)</h2>', html)
    author = re.findall(r'<span itemprop="name">(.*?)</span>', html)
    author_bio = re.findall(r'<section class="author-bio-top">.*?</a>(.*?)</section>', html, re.DOTALL)
    publish_date = re.findall(r'<time class=".*">(.*?)</time>', html, re.DOTALL)
    images = re.findall(r'<img.*alt="(.*?)">', html)
    figure_captions = re.findall(r'<div class="caption">(.*?)</div>', html, re.DOTALL)
    # TODO fix this getting only first paragraph
    content = re.findall(r'<section class="article-guts">.*?<p>(.*?)</p>.*?</section>', html, re.DOTALL)
    
    print(
        {
            'title': title[0] if title else None,
            'subtitle': subtitle[0] if subtitle else None,
            'author': author[0] if author else None,
            'author_bio': author_bio[0].strip() if author_bio else None,
            'publish_date': publish_date[0] if publish_date else None,
            'figures': images + [cap.strip() for cap in figure_captions ],
            'content': content
        }
    )