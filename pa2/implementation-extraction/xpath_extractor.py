from lxml import etree

def extract_data_from_rtvslo(html):
    tree = etree.HTML(html)
    title = tree.xpath('//h1/text()')
    subtitle = tree.xpath('//div[@class="subtitle"]/text()')
    lead = tree.xpath('//p[@class="lead"]/text()')
    author = tree.xpath('//div[@class="author-name"]/text()')
    publish_date = tree.xpath('//div[@class="publish-meta"]/text()')
    content = tree.xpath('//div[@class="article-body"]//p/text()')
    highlights = tree.xpath('//ul[@class="emphasis-list"]//li/text()')
    
    key_data = tree.xpath('//article[@class="article"]//p[not(@class)]/text()')
    
    print( 
        {
            'title': title[0] if title else None,
            'subtitle': subtitle[0] if subtitle else None,
            'lead': lead[0] if lead else None,
            'author': author[0] if author else None,
            'publish_date': publish_date[0].strip() if publish_date else None,
            'highlights': highlights if highlights else None,
            'key_data': [data.replace('-', ' ').strip() for data in key_data],
            'content': content if content else None
        })
    
def extract_data_from_overstock(html):
    # Find card by background color, it's an ugly hack but it works
    tree = etree.HTML(html)
    list_prices = tree.xpath('//tr[@bgcolor="#ffffff" or @bgcolor="#dddddd"]//s/text()')
    actual_prices = tree.xpath('//tr[@bgcolor="#ffffff" or @bgcolor="#dddddd"]//span[@class="bigred"]/b/text()')
    item_descriptions = tree.xpath('//tr[@bgcolor="#ffffff" or @bgcolor="#dddddd"]//span[@class="normal"]/text()')
    item_titles = tree.xpath('//tr[@bgcolor="#ffffff" or @bgcolor="#dddddd"]//a[@href]/b/text()')
    for i in range(len(item_descriptions)):
        print(
            {
                'item_title': item_titles[i],
                'list_price': list_prices[i],
                'actual_price': actual_prices[i],
                'item_description': item_descriptions[i]
            }
        )

def extract_data_from_arstechnica(html):
    tree = etree.HTML(html)
    title = tree.xpath('//h1[@itemprop="headline"]//text()')
    subtitle = tree.xpath('//h2[@itemprop="description"]/text()')
    author = tree.xpath('//span[@itemprop="name"]/text()')
    author_bio = tree.xpath('//section[@class="author-bio-top"]/a/following-sibling::text()')
    publish_date = tree.xpath('//p[@class="byline"]//time/text()')
    images = tree.xpath('//img[@alt]/@alt')
    figure_captions = tree.xpath('//div[@class="caption"]/text()')
    content = tree.xpath('//section[@class="article-guts"]//p/text()')
    
    print(
        {
            'title': ''.join(title) if title else None,
            'subtitle': subtitle[0] if subtitle else None,
            'author': author[0] if author else None,
            'author_bio': author_bio[0].strip() if author_bio else None,
            'publish_date': publish_date[0] if publish_date else None,
            'figures': images + [cap.strip() for cap in figure_captions ],
            'content': content
        })