
from bs4 import BeautifulSoup
import pylcs

class Layout:
    def __init__(self, page):
        self.blocks = generate_layout(page)
    
    def find_common(self, blocks_1, blocks_2):
        #common_blocks=[]
        #min_len = min(len(blocks_1), len(blocks_2))
        #for blocks in range(min_len):
        #    if blocks_1[blocks].tag == blocks_2[blocks].tag:
        #        common_blocks.append(blocks_1[blocks])
        number=lcs(blocks_1,blocks_2)
        common_blocks=print_lcs(blocks_1,blocks_2,number)

        return common_blocks

    
class Block:    
    def __init__(self, tag, content, xpath):
        self.tag = tag
        self.content = content
        self.xpath = xpath
    
    def diff_score(self, other):
        lcs = pylcs.lcs(self.content, other.content)
        w1 = len(self.content)
        w2 = len(other.content)
        return (w1+w2 - 2*lcs) / (w1+w2)


def generate_layout(page):
    page_soup = BeautifulSoup(page, 'html.parser')
    # Keep only text elements
    layout = []
    for tag in page_soup.find_all():
        if tag.name in ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'a', 'li', 'ul', 'ol', 'table', 'tr', 'td', 'th']:
            tag_content = tag.text.strip()
            if len(tag_content) > 0:
                layout.append(Block(tag.name, tag_content, xpath_soup(tag)))
    return layout


def lcs(u, v):
    c=[[-1]*(len(v)+1) for _ in range(len(u)+1)]

    for i in range(len(u)+1):
        c[i][len(v)]=0
    for j in range(len(v)):
        c[len(u)][j]=0

    for i in range(len(u)-1, -1, -1):
        for j in range(len(v)-1, -1, -1):
            if u[i].tag==v[j].tag:
                c[i][j]=1+c[i+1][j+1]
            else:
                c[i][j]=max(c[i+1][j], c[i][j+1])

    return c


def print_lcs(u, v, c):
    common_blocks=[]
    i=j=0
    while not (i==len(u) or j==len(v)):
        if u[i].tag==v[j].tag:
            common_blocks.append(u[i])
            i+=1
            j+=1
        elif c[i][j+1]>c[i+1][j]:
            j+=1
        else:
            i+=1
    return common_blocks


# Taken from https://gist.github.com/ergoithz/6cf043e3fdedd1b94fcf
def xpath_soup(element):
    # type: (typing.Union[bs4.element.Tag, bs4.element.NavigableString]) -> str
    """
    Generate xpath from BeautifulSoup4 element.
    :param element: BeautifulSoup4 element.
    :type element: bs4.element.Tag or bs4.element.NavigableString
    :return: xpath as string
    :rtype: str
    Usage
    -----
    >>> import bs4
    >>> html = (
    ...     '<html><head><title>title</title></head>'
    ...     '<body><p>p <i>1</i></p><p>p <i>2</i></p></body></html>'
    ...     )
    >>> soup = bs4.BeautifulSoup(html, 'html.parser')
    >>> xpath_soup(soup.html.body.p.i)
    '/html/body/p[1]/i'
    >>> import bs4
    >>> xml = (
    ...     '<?xml version="1.0" encoding="UTF-8"?>'
    ...     '<doc xmlns:ns1="http://localhost/ns1"'
    ...     '     xmlns:ns2="http://localhost/ns2">'
    ...     '<ns1:elm/><ns2:elm/><ns2:elm/></doc>'
    ...     )
    >>> soup = bs4.BeautifulSoup(xml, 'lxml-xml')
    >>> xpath_soup(soup.doc.find('ns2:elm').next_sibling)
    '/doc/ns2:elm[2]'
    """
    components = []
    target = element if element.name else element.parent
    for node in (target, *target.parents)[-2::-1]:  # type: bs4.element.Tag
        tag = '%s:%s' % (node.prefix, node.name) if node.prefix else node.name
        siblings = node.parent.find_all(tag, recursive=False)
        components.append(tag if len(siblings) == 1 else '%s[%d]' % (tag, next(
            index
            for index, sibling in enumerate(siblings, 1)
            if sibling is node
            )))
    return '/%s' % '/'.join(components)