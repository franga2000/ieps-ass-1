
from analyze import Layout
import xml.etree.ElementTree as ET

def extractor(page1, page2):

	layout1 = Layout(page1)
	layout2 = Layout(page2)

	# Generate layout pattern
	layout_pattern_1 = layout1.find_common(layout1.blocks, layout2.blocks)
	layout_pattern_2 = layout2.find_common(layout2.blocks, layout1.blocks)

	# Filter out static elements
	difscores=[]
	filtered1=[]
	filtered2=[]
	for i in range(len(layout_pattern_1)):
		block1 = layout_pattern_1[i]
		block2 = layout_pattern_2[i]
		if block1.diff_score(block2) > 0.50:
			difscores.append(block1.diff_score(block2))
			filtered1.append(block1)
			filtered2.append(block2)

	# Extract main and title text
	title = find_title(layout_pattern_1)
	output = { 'title': title, 'main': []}
	print('Title: ', title.xpath if title else "/")
	blocks, scores = main_blocks(difscores,filtered1,filtered2)
	for block in blocks:
		output['main'].append(block)
	for i in range(len(output['main'])):
		print('Main: ', difscores[i], scores[i], output['main'][i].xpath)

def main_blocks(diffScores, layout1, layout2):
	main_score_threshold = 130
	main_blocks=[]
	main_scores = []
	for i in range(len(diffScores)):
		weight1=len(layout1[i].content)
		weight2=len(layout2[i].content)
		mainScore = diffScores[i]*(weight1+weight2)/2
		if mainScore > main_score_threshold:
			main_blocks.append(layout1[i])
			main_scores.append(mainScore)
	return main_blocks, main_scores

def find_title(layout):
	# Return first H1 tag you can find
	# This works for news sites
	for i in range(len(layout)):
		if layout[i].tag == 'h1':
			return layout[i]
	return False