
from enum import auto
import sys
import regex_extractor
import xpath_extractor
import automatic_extractor

mode = sys.argv[1]
if mode not in ['A', 'B', 'C', 'E']:
    print("Invalid mode")
    sys.exit(1)

with open("../input-extraction/rtvslo.si/AudiA6.html", "r") as f:
    audi = f.read()
with open("../input-extraction/rtvslo.si/VolvoXC40.html", "r") as f:
    volvo = f.read()

with open("../input-extraction/overstock.com/jewelry01.html", "r", encoding='windows-1252') as f:
    jewelry_1 = f.read()

with open("../input-extraction/overstock.com/jewelry02.html", "r", encoding='windows-1252') as f:
    jewelry_2 = f.read()

with open("../input-extraction/arstechnica.com/MiniSettlers.html", "r") as f:
    mini_settlers = f.read()

with open("../input-extraction/arstechnica.com/SixtyFour.html", "r") as f:
    sixty_four = f.read()


if mode == 'A':

    regex_extractor.extract_data_from_rtvslo(audi)
    regex_extractor.extract_data_from_rtvslo(volvo)

    regex_extractor.extract_data_from_overstock(jewelry_1)
    regex_extractor.extract_data_from_overstock(jewelry_2)

    regex_extractor.extract_data_from_arstechnica(mini_settlers)
    regex_extractor.extract_data_from_arstechnica(sixty_four)
elif mode == 'B':
    xpath_extractor.extract_data_from_rtvslo(audi)
    xpath_extractor.extract_data_from_rtvslo(volvo)

    xpath_extractor.extract_data_from_overstock(jewelry_1)
    xpath_extractor.extract_data_from_overstock(jewelry_2)

    xpath_extractor.extract_data_from_arstechnica(mini_settlers)
    xpath_extractor.extract_data_from_arstechnica(sixty_four)
else:
    # automatic_extractor.extractor(volvo, audi)
    # automatic_extractor.extractor(audi, volvo)
    automatic_extractor.extractor(jewelry_1, jewelry_2)
    # automatic_extractor.extractor(mini_settlers, sixty_four)
    # automatic_extractor.extractor(sixty_four, mini_settlers)
    pass