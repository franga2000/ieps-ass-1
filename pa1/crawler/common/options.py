import os

DB_HOST = 'postgres'
DB_USER = 'postgres'
DB_PASSWORD = 'postgres'
DB_NAME = 'ieps_' + os.environ['DOWNLOADER_MODE']