from enum import Enum


class DataType(str, Enum):
	PDF = 'PDF'
	DOC = 'DOC'
	DOCX = 'DOCX'
	PPT = 'PPT'
	PPTX = 'PPTX'
	UNKNOWN = 'UNKNOWN'

class PageType(str, Enum):
	ERROR = 'ERROR'
	PROCESSING = 'PROCESSING'
	HTML = 'HTML'
	BINARY = 'BINARY'
	DUPLICATE = 'DUPLICATE'
	FRONTIER = 'FRONTIER'