import logging
from peewee import *

from .options import *

logger = logging.getLogger('database')

logger.debug(f'Connecting to database {DB_NAME} on {DB_HOST} as {DB_USER}')

db = PostgresqlDatabase(DB_NAME, user=DB_USER, password=DB_PASSWORD, host=DB_HOST)
db.execute_sql('SET search_path TO crawldb;')

class BaseModel(Model):
    class Meta:
        database = db

class DataType(BaseModel):
    code = CharField(max_length=20, primary_key=True)

class PageType(BaseModel):
    code = CharField(max_length=20, primary_key=True)

class IP(BaseModel):
    ip = CharField(max_length=15, primary_key=True)
    last_fetched = DateTimeField(null=True)

class Site(BaseModel):
    id = AutoField(primary_key=True)
    domain = CharField(max_length=500, null=True)
    robots_content = TextField(null=True)
    sitemap_content = TextField(null=True)
    ip = ForeignKeyField(IP, backref='sites', null=True, column_name='ip')
    last_accessed = DateTimeField(null=True)
    crawl_delay = IntegerField(default=5)

class Page(BaseModel):
    id = AutoField(primary_key=True)
    site_id = ForeignKeyField(Site, backref='pages')
    page_type_code = ForeignKeyField(PageType, backref='pages', column_name='page_type_code')
    url = CharField(max_length=3000, unique=True)
    html_content = TextField(null=True)
    http_status_code = IntegerField(null=True)
    accessed_time = DateTimeField(null=True)
    content_hash = BlobField(null=True, index=True)

class PageData(BaseModel):
    id = AutoField(primary_key=True)
    page_id = ForeignKeyField(Page, backref='page_data')
    data_type_code = ForeignKeyField(DataType, backref='page_data', null=True)
    data = BlobField(null=True)

class Image(BaseModel):
    id = AutoField(primary_key=True)
    page_id = ForeignKeyField(Page, backref='images')
    filename = CharField(max_length=255)
    content_type = CharField(max_length=50)
    data = BlobField()
    accessed_time = DateTimeField()

class Link(BaseModel):
    from_page = ForeignKeyField(Page, backref='outgoing_links', column_name='from_page')
    to_page = ForeignKeyField(Page, backref='incoming_links', column_name='to_page')

