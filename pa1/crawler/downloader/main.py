from datetime import datetime
import logging
from pathlib import Path
import socket
import colorlog
from urllib.parse import urlparse

import requests
import tldextract
import mimetypes

from downloader.sitemap import get_urls_from_sitemap

handler = colorlog.StreamHandler()
handler.setFormatter(colorlog.ColoredFormatter('%(log_color)s%(levelname)s:%(name)s:%(message)s'))

logging.basicConfig(level=logging.INFO, handlers=[handler])

import bs4
from common.enums import DataType, PageType
from common.models import Image, Link, Page, PageData, Site
from downloader.fetch import canonicalize_url, fetch_page, hash_html, pick_next_page
from downloader.sites import get_or_make_site
from downloader.robots import can_follow

from selenium import webdriver


import os
from time import sleep

logger = logging.getLogger('downloader')


START_URLS_FILE = Path(__file__).parent.parent / 'start_urls.txt'
mode= os.environ["DOWNLOADER_MODE"]
driver = False

initialized = False

def insert_initial_pages():
    global initialized
    initialized = True

    urls = START_URLS_FILE.read_text().strip().split('\n')
    add_frontier_page_many(urls)
    # for url in urls:
    # add_frontier_page(url)

def add_frontier_page(url):
    url = canonicalize_url(None, url)
    site = get_or_make_site(tldextract.extract(url).fqdn)

    page, created = Page.get_or_create(
        url=url,
        defaults=dict(
            url=url,
            site_id=site,
            page_type_code=PageType.FRONTIER,
        )
    )
    if created:
        logger.info(f'Added page to frontier: {page.url}')
    else:
        logger.info(f'Page already in frontier: {page.url}')
    return page

def add_frontier_page_many(urls, from_page=None):
    entries=[]

    for url in urls:
        url = canonicalize_url(None, url)
        site=get_or_make_site(tldextract.extract(url).fqdn)
        entries.append([url, site, PageType.FRONTIER])

    with Page._meta.database.atomic():
        query = Page.insert_many(entries, ["url","site_id","page_type_code"]).on_conflict_ignore()
        query.execute()

        if from_page:
            pages_to_link = Page.select().where(Page.url.in_(urls))
            query = Link.insert_many(   
                [{'from_page': from_page, 'to_page': to_page} for to_page in pages_to_link],
                fields=[Link.from_page, Link.to_page]
            ).on_conflict_ignore()
            query.execute()

    logger.info(f'Pages added to frontier.')

def process_html(page: Page, html : str):
    parsed_html = bs4.BeautifulSoup(html, 'html.parser')
    
    # Extract images
    imgs = parsed_html.find_all('img')
    imgs_to_add = []
    for img in imgs:
        src = img.get('src')
        src = canonicalize_url(page, src)
        imgs_to_add.append(src.split('/')[-1][:255])

    with Image._meta.database.atomic():
        logger.info(f'Adding {len(imgs_to_add)} images for page {page.url}')
        query = Image.insert_many(
            [{'page_id': page, 
              'filename': img,
              'content_type': 'image',
              'data': b'',
              'accessed_time': datetime.now()} for img in imgs_to_add],
            fields=[Image.page_id, Image.filename, Image.content_type, Image.data, Image.accessed_time]
        ).on_conflict_ignore()
        query.execute()

            
    # Extract links
    links = parsed_html.find_all('a')
    links_to_add = []
    for link in links:
        href = link.get('href')
        if href:
            # Ignore tel and mailto links
            if href.startswith('mailto:') or href.startswith('tel:'):
                continue
            link = canonicalize_url(page, href)
            if can_follow(link):
                links_to_add.append(link)
            # else:
            #     logger.info(f'Not following link {link}')
    add_frontier_page_many(links_to_add, from_page=page)

    # Clean html before storing
    for tag in parsed_html(['img', 'link', 'style', 'script']):
        tag.decompose()
    page.html_content = parsed_html.prettify()
    page.save()

def process_binary(page: Page):
    logging.info(f'Page {page.url} is not HTML')
    data_type = mimetypes.guess_type(page.url)[0]
    # logging.info(f'Guessed data type: {data_type}')
    page.page_type_code = PageType.BINARY
    data_type_code = DataType.UNKNOWN
    if data_type == 'application/pdf':
        data_type_code = DataType.PDF
    elif data_type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
        data_type_code = DataType.DOCX
    elif data_type == 'application/msword':
        data_type_code = DataType.DOC
    elif data_type == 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
        data_type_code = DataType.PPTX
    elif data_type == 'application/vnd.ms-powerpoint':
        data_type_code = DataType.PPT
    PageData.create(
        page_id=page,
        data_type_code=data_type_code,
        data='')
    page.save()


def process_page(page: Page, html: str):
    page_hash = hash_html(html)
    page.content_hash = page_hash
    duplicate = Page.get_or_none(content_hash=page_hash)
    if duplicate:
        page.page_type_code = PageType.DUPLICATE
        Link.create(from_page=page, to_page=duplicate)
    else:
        page.page_type_code = PageType.HTML
        process_html(page, html)
    page.save()

def add_sitemap_pages(site: Site):
    # Don't re-fetch sitemap if we already have it
    if site.sitemap_content or site.sitemap_content == '':
        return
    urls = get_urls_from_sitemap(site.domain)
    site.sitemap_content = '\n'.join(urls)
    # Prevent fetching sitemaps that were already checked to be empty
    if not urls:
        # logger.info(f'No sitemap found for {site.domain}')
        site.sitemap_content = ''
    site.save()

    add_frontier_page_many(urls)


def download_loop():
    socket.setdefaulttimeout(10)
    global driver
    if not initialized:
        insert_initial_pages()

    if mode == "render":
        # Disable cookies in selenium
        options = webdriver.ChromeOptions()
        options.add_argument('--headless')

        # disable image rendering
        options.add_experimental_option("prefs", {
            # block image loading
            "profile.managed_default_content_settings.images": 2,
        })

        driver = webdriver.Chrome(options=options)

    while True:
        try:
            page = pick_next_page()
            if not page:
                # If not pages to currently pick, wait a bit
                sleep(1)
                #logger.info('No pages to currently pick')
                continue
            try:
                
                head = requests.head(page.url, allow_redirects=True, timeout=10)
                if head.status_code >= 400:
                    page.accessed_time = datetime.now()
                    page.http_status_code = head.status_code
                    page.page_type_code = PageType.ERROR
                    page.save()
                    continue
                is_html = 'html' in head.headers['Content-Type']
                if is_html:
                    downloaded = fetch_page(page, driver)
                    if not downloaded:
                        continue
                    add_sitemap_pages(Site.get(Site.id == page.site_id))
                    process_page(page, downloaded)
                else:
                    process_binary(page)
            except:
                logger.exception(f'Failed to download page {page.url}')
                page.accessed_time = datetime.now()
                page.http_status_code = 0
                page.page_type_code = PageType.ERROR
                page.save()
                continue
        except:
            logger.exception(f'Failed in main loop')
    
    
        

if __name__ == '__main__':
    download_loop()
