
from venv import logger
from bs4 import BeautifulSoup as soup
import requests
import tldextract
from .usp.tree import sitemap_tree_for_homepage

def get_sitemap_from_url(sitemap_url: str):
    if not sitemap_url.endswith('.xml'):
        return False
    return requests.get(sitemap_url).text
    
def get_urls_from_sitemap(url: str):
        tld = tldextract.extract(url)
        tree = sitemap_tree_for_homepage(f'https://{tld.fqdn}')
        return [page.url for page in tree.all_pages()]