
import logging
import os

import socket
from urllib.parse import urlparse

import requests
from common.models import Site, IP
from urllib.robotparser import RobotFileParser
from datetime import datetime

SITE_CACHE = {}

logger = logging.getLogger('sites')

def get_or_make_site(domain: str) -> Site:
    if domain in SITE_CACHE:
        return SITE_CACHE[domain]
    
    with Site._meta.database.atomic():
        try:
            site = Site.get(Site.domain == domain)

        except Site.DoesNotExist:

            address = socket.gethostbyname(domain)
            ip, _ = IP.get_or_create(
                ip=address,
                defaults=dict(
                    ip=address,
                    last_fetched=datetime.now(),
                )
            )
            site = Site(
                domain=domain,
                ip=ip,
                last_accessed=datetime.now(),
            )
            try:
                logger.debug(f'Getting robots: https://{domain}/robots.txt')
                # TODO parse sitemap content and use it when adding pages to frontier
                scheme = urlparse(domain).scheme
                if not scheme:
                    scheme = 'https'
                rp = RobotFileParser(f'{scheme}://{domain}/robots.txt')
                rp.read()
                site.robots_content = str(rp)
                ua = os.environ['USER_AGENT']
                if rp.crawl_delay(ua):
                    site.crawl_delay = rp.crawl_delay(ua)
            except:
                logger.exception(f'Failed to fetch robots.txt for {domain}')
            
            site.save()
        
    SITE_CACHE[domain] = site
    return site