import logging
from urllib import request
from urllib.parse import urlparse
from urllib.robotparser import RobotFileParser

from requests import session
import requests

from downloader.sites import get_or_make_site

import os
import tldextract

logger = logging.getLogger('robots')

def can_follow(url: str) -> bool:
    try:
        allowed_domains = os.environ['ALLOWED_DOMAINS'].split(',')
        tld = tldextract.extract(url)
        domain = tld.fqdn
        # Blacklist evem
        if domain == 'evem.gov.si':
            return False
        if not (tld.registered_domain in allowed_domains):
            logger.info(f'Domain {domain} not in allowed domains')
            return False
    
        get_or_make_site(domain)
        try :
            scheme = urlparse(url).scheme
            if not scheme:
                scheme = 'https'
            robots_url = f'{scheme}://{domain}/robots.txt'
                
            rp = RobotFileParser(robots_url)
            rp.read()
            return rp.can_fetch(os.environ['USER_AGENT'], url)
        except:
            logger.info(f'Failed to parse robots.txt at {robots_url}')
            return True
    except:
        logger.info(f'Failed to check if we can follow link, assuming no: {url}')
        return False
