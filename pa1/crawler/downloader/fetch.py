from ast import parse
import logging
import hyperlink
import requests
import xxhash
from urllib.parse import urljoin, urlparse

from common.enums import PageType
from common.models import Page, Site, IP

import datetime
import os

from peewee import fn, SQL

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options as FirefoxOptions


logger = logging.getLogger('fetch')

def hash_html(html: bytes) -> bytes:
    h = xxhash.xxh64()
    h.update(html)
    return h.digest()


def pick_next_page() -> Page:
    next_page = Page.select().join(Site)\
                             .join(IP).where(Page.page_type_code == PageType.FRONTIER,
                                            fn.GREATEST(Site.last_accessed, IP.last_fetched)
                                            + (SQL("INTERVAL '1 second'") * Site.crawl_delay)
                                            < datetime.datetime.now()).first()
    if next_page:
        next_page.page_type_code = PageType.PROCESSING
        next_page.save()
    return next_page

session = requests.Session()
session.headers['User-Agent'] = os.environ['USER_AGENT']

def fetch_page(page: Page, driver: webdriver) -> requests.Response:
    logger.info(f'Fetching {page.url}')

     # Update access time
    site = Site.get(Site.id == page.site_id)
    site.last_accessed = datetime.datetime.now()
    site.save()

    # Update last fetched time
    ip = IP.get(IP.ip == site.ip)
    ip.last_fetched = datetime.datetime.now()
    ip.save()

    page.accessed_time = datetime.datetime.now()
    page.save()

    try:
        if driver:
            driver.delete_all_cookies()
            logger.info(f'Fetching selenium page {page.url}')
            driver.get(page.url)
            return driver.page_source
        else:
            logger.info(f'Fetching requests page {page.url}')
            return session.get(page.url, allow_redirects=True, timeout=10).text
    except Exception as e:
        logger.exception(f'Failed to fetch {page.url}')
        return ''

    
def canonicalize_url(parent_page: Page, url: str) -> str:
    normalized_url = hyperlink.URL.from_text(url).normalize()
    parsed_url = urlparse(normalized_url.to_text())
    path = parsed_url.path
    # Remove print and javascript from url
    if '/javascript(0)' in path:
        path = path.replace('/javascript(0)', '')
    if '/print' in path:
        path = path.split('/print', 1)[0]
    if not normalized_url.absolute:
        normalized_parent_url = hyperlink.URL.from_text(parent_page.url).normalize()
        parsed_parent_url = urlparse(normalized_parent_url.to_text())
        return f'{parsed_parent_url.scheme}://{parsed_parent_url.netloc}{urljoin(parsed_parent_url.path, path)}'
    return f'{parsed_url.scheme}://{parsed_url.netloc}{path}'